FROM gitlab/gitlab-runner:latest

ENV DEBIAN_FRONTEND=noninteractive

# Ubuntu dependencies for generating database docs
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends \
    openjdk-17-jre \
    graphviz \
    openssh-client
    
# Ubuntu dependencies for testing and deploying R packages and shiny apps
RUN apt-get install -y --no-install-recommends \
    build-essential \ 
    libgit2-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    libcairo2-dev \
    libpng-dev \
    libtiff-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libfontconfig1-dev \
    libxt-dev \
    libmagick++-dev \
    pandoc \
    r-base
    
# R dependencies for testing and deploying R packages, shiny apps and R markdown reports
RUN R -e "install.packages(c('remotes', 'shiny', 'shinytest', 'usethis', 'rmarkdown', 'testthat', 'tidyverse'),  repos='http://cran.rstudio.com/')"
RUN R -e  "shinytest::installDependencies()"
